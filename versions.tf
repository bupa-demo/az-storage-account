terraform {
  required_version = ">= 1.3.1"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.39.1"
    }
  }
}

provider "azurerm" {
  subscription_id = "0761d732-cb61-40d3-9def-f44ea634d9b9"
  features {}  
}