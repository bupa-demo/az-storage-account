output "storage_account" {
  value       = azurerm_storage_account.storage_account
  sensitive   = true
  description = "The Storage Account object."
}

output "storage_account_name" {
  value       = azurerm_storage_account.storage_account.name
  description = "The name of the Storage Account."
}

output "storage_account_id" {
  value       = azurerm_storage_account.storage_account.id
  description = "The ID of the Storage Account."
}

output "primary_access_key" {
  value       = azurerm_storage_account.storage_account.primary_access_key
  sensitive   = true
  description = "The primary access key for the storage account."
}

output "secondary_access_key" {
  value       = azurerm_storage_account.storage_account.secondary_access_key
  sensitive   = true
  description = "The secondary access key for the storage account."
}

output "primary_blob_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_blob_endpoint
  description = "The endpoint URL for blob storage in the primary location."
}

output "primary_blob_host" {
  value       = azurerm_storage_account.storage_account.primary_blob_host
  description = "The endpoint host for blob storage in the primary location."
}

output "secondary_blob_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_blob_endpoint
  description = "The endpoint URL for blob storage in the secondary location."
}

output "secondary_blob_host" {
  value       = azurerm_storage_account.storage_account.secondary_blob_host
  description = "The endpoint host for blob storage in the secondary location."
}

output "primary_queue_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_queue_endpoint
  description = "The endpoint URL for queue storage in the primary location."
}

output "secondary_queue_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_queue_endpoint
  description = "The endpoint URL for queue storage in the secondary location."
}

output "primary_table_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_table_endpoint
  description = "The endpoint URL for table storage in the primary location."
}

output "secondary_table_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_table_endpoint
  description = "The endpoint URL for table storage in the secondary location."
}

output "primary_file_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_file_endpoint
  description = "The endpoint URL for file storage in the primary location."
}

output "secondary_file_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_file_endpoint
  description = "The endpoint URL for file storage in the secondary location."
}

output "primary_dfs_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_dfs_endpoint
  description = "The endpoint URL for DFS storage in the primary location."
}

output "secondary_dfs_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_dfs_endpoint
  description = "The endpoint URL for DFS storage in the secondary location."
}

output "primary_web_host" {
  value       = azurerm_storage_account.storage_account.primary_web_host
  description = "Hostname with port for web storage in the primary location."
}

output "primary_web_endpoint" {
  value       = azurerm_storage_account.storage_account.primary_web_endpoint
  description = "The endpoint URL for web storage in the primary location."
}

output "secondary_web_host" {
  value       = azurerm_storage_account.storage_account.secondary_web_host
  description = "Hostname with port for web storage in the secondary location."
}

output "secondary_web_endpoint" {
  value       = azurerm_storage_account.storage_account.secondary_web_endpoint
  description = "The endpoint URL for web storage in the secondary location."
}

output "primary_connection_string" {
  value       = azurerm_storage_account.storage_account.primary_connection_string
  sensitive   = true
  description = "The connection string associated with the primary location."
}

output "secondary_connection_string" {
  value       = azurerm_storage_account.storage_account.secondary_connection_string
  sensitive   = true
  description = "The connection string associated with the secondary location."
}

output "primary_blob_connection_string" {
  value       = azurerm_storage_account.storage_account.primary_blob_connection_string
  sensitive   = true
  description = "The connection string associated with the primary blob location."
}

output "secondary_blob_connection_string" {
  value       = azurerm_storage_account.storage_account.secondary_blob_connection_string
  sensitive   = true
  description = "The connection string associated with the secondary blob location."
}

output "principal_id" {
  value       = azurerm_storage_account.storage_account.identity.0.principal_id
  description = "The Principal ID for the Service Principal associated with the Identity of this Storage Account."
}

output "tenant_id" {
  value       = azurerm_storage_account.storage_account.identity.0.tenant_id
  description = "The Tenant ID for the Service Principal associated with the Identity of this Storage Account."
}